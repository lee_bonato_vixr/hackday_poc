﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackDay.Models
{
    public class Auth0Model
    {
        
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string Domain { get; set; }
        public string Audience
        {
            get
            {
                return string.Format("https://{0}/api/v2/", Domain);
            }
        }
        public string DBName { get; set; }
         
    }
}
