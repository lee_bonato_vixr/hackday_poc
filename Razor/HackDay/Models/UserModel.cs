﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HackDay
{
    public class UserModel
    {
        [Required]
        public string Email { get; set; }
        public string Upn { get; set; }
        public bool SamActive { get; set; }
        public bool WorkflowActive { get; set; }
        public string ResultMessage { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
