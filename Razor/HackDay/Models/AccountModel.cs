﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HackDay.Classes
{
    public class AccountModel
    {
        public string SamApiKey { get; set; }
        public string SamContentType { get; set; }
        public string SamDomain { get; set; }
    }
}
