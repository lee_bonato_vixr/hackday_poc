﻿using HackDay.Helpers;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HackDay.Pages
{
     
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        AccountHelper accountHelper = new AccountHelper();
        Auth0Helper auth0Helper = new Auth0Helper();
        Auth0User auth0user = new Auth0User();
        SamUser samUser = new SamUser();
        AccountResponse response = new AccountResponse();

        public ChallengeResult OnGet(string returnUrl = "/")
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Challenge(new AuthenticationProperties() { RedirectUri = returnUrl }, "Auth0");
            }
            SearchUser = new UserModel();

            foreach (var claim in User.Claims)
            {
                if (claim.Type  == "nickname")
                {
                    ViewData["UserName"] = claim.Value; 
                }
            }
            return null;
        }

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnPostGetUser()
        {
            GetUser();
        }


        private void GetUser()
        {
            auth0user = auth0Helper.GetUserByEmail(SearchUser.Email);
            samUser = accountHelper.GetSamUserByEmail(SearchUser.Email);

            if (auth0user != null)
            {
                SearchUser.Upn = auth0user.user_id;
            }
            else
            {
                SearchUser.ResultMessage = "Auth0 User not found";
            }

            if (samUser != null)
            {
                
                SearchUser.SamActive = samUser.Active && samUser.UPN.Equals(SearchUser.Upn);

                SearchUser.FirstName = samUser.FirstName;
                SearchUser.Surname = samUser.Surname;
            }
            else
            {
                SearchUser.ResultMessage = "SAM User not found";
            }
        }

        public void OnPostUpdate()
        {
            if (samUser != null)
            {
                samUser.Email = SearchUser.Email;
                samUser.UPN = SearchUser.Upn;
                samUser.Active = SearchUser.SamActive;
                response = accountHelper.SetSamUserActive(samUser);

                if (response.IsSuccessful)
                {
                    SearchUser.ResultMessage = "SAM User record updated successfully";

                    GetUser();
                }
                else
                {
                    SearchUser.ResultMessage = "SAM User record could not be updated";
                }
            }
        }

        [BindProperty]
        public UserModel SearchUser { get; set; }

    }
}
