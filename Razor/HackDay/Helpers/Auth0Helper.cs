﻿using System;
using RestSharp;
using Newtonsoft.Json;
using System.Net;
using System.Collections.Generic;
using System.IO;
using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using Microsoft.Extensions.Configuration;
using HackDay.Models;

namespace HackDay.Helpers
{



    public class Auth0Helper
    {

        public Auth0Model _auth0 { get; set; }

        // Constructor
        public Auth0Helper()
        {
            _auth0 = LoadFromParamStore();
            if (_auth0 == null)
            {
                _auth0 = LoadFromFile();
            }
        }

        /// <summary>
        /// load config from the parameter store
        /// </summary>
        private Auth0Model LoadFromParamStore()
        {
            Auth0Model auth0 = null;

            // get from app setting in the config
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            var appSetting = root.GetSection("AppSettings");
            var paramStoreProfile = appSetting["ParamStoreProfile"];
            var paramStoreKey = appSetting["ParamStoreKey"];

            var request = new GetParametersRequest
            {
                Names = new List<string> { paramStoreKey }
            };

            try
            {
                CredentialProfile basicProfile;
                AWSCredentials awsCredentials;
                var sharedFile = new SharedCredentialsFile();
                if (sharedFile.TryGetProfile(paramStoreProfile, out basicProfile) &&
                    AWSCredentialsFactory.TryGetAWSCredentials(basicProfile, sharedFile, out awsCredentials))
                {
                    using (var ssm = new AmazonSimpleSystemsManagementClient(awsCredentials, RegionEndpoint.APSoutheast2))
                    {
                        var response = ssm.GetParametersAsync(request).Result;
                        if (response.Parameters.Count > 0)
                        {
                            string retValue = response.Parameters.ToArray()[0].Value;
                            auth0 = JsonConvert.DeserializeObject<Auth0Model>(retValue);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                auth0 = null;
            }
            return auth0;
        }

        /// <summary>
        /// load config from file
        /// </summary>
        private Auth0Model LoadFromFile()
        {
            Auth0Model auth0 = null;
            string configFileName = @"config.json";
            string projectRootPath = AppDomain.CurrentDomain.BaseDirectory;
            string configFileAndPath = projectRootPath + configFileName;

            try
            {
                using (StreamReader r = new StreamReader(configFileAndPath))
                {
                    string retValue = r.ReadToEnd();
                    auth0 = JsonConvert.DeserializeObject<Auth0Model>(retValue);
                    //auth0 = new Auth0Model();
                    //auth0.Domain = auth0Config.Domain;
                    //auth0.ClientId = auth0Config.ClientId;
                    //auth0.ClientSecret = auth0Config.ClientSecret;
                    //auth0.DBName = auth0Config.DBName;
                }
            }
            catch (Exception)
            {
                auth0 = null;
            }
            return auth0;
        }

        /// <summary>
        /// Creates and returns a token for API calls
        /// </summary>
        private Auth0Token CreateToken()
        {
            // Make the POST request to the Auth0 domain for a token
            var client = new RestClient($"https://{_auth0.Domain}/oauth/token");
            var postRequest = new RestRequest(Method.POST);
            postRequest.AddHeader("content-type", "application/json");
            postRequest.AddParameter("application/json", $"{{\"client_id\":\"{_auth0.ClientId}\",\"client_secret\":\"{_auth0.ClientSecret}\",\"audience\":\"{_auth0.Audience}\",\"grant_type\":\"client_credentials\"}}"
                , RestSharp.ParameterType.RequestBody);

            IRestResponse response = client.Execute(postRequest);

            // Get the JSON reponse and store as a Token object
            string responseContent = response.Content;
            Auth0Token token = JsonConvert.DeserializeObject<Auth0Token>(responseContent);

            // Return Token object
            return token;
        }

        /// <summary>
        /// Searches for an Auth0 User by email address and returns User object
        /// </summary>
        public Auth0User GetUserByEmail(string emailAddress)
        {
            string usersByEmailApiPath = $"https://{_auth0.Domain}/api/v2/users-by-email?email={emailAddress}";

            // Create an authorisation token for the API request
            var token = CreateToken();

            // Make the GET request to the Auth0 'Users by email' API path
            var client = new RestClient(usersByEmailApiPath);
            var getRequest = new RestRequest(Method.GET);
            getRequest.AddHeader("authorization", $"Bearer {token.access_token}");

            IRestResponse response = client.Execute(getRequest);

            // Get the JSON reponse and store as a User object
            var userList = JsonConvert.DeserializeObject<Auth0User[]>(response.Content);
            Auth0User user = null;
            if (userList != null && userList.Length == 1)
            {
                user = userList[0];
            }

            // Return User object
            return user;
        }

        /// <summary>
        /// Searches for an Auth0 User by User ID and returns User object
        /// </summary>
        public Auth0User GetUserById(string userId)
        {
            string usersByIdApiPath = $"https://{_auth0.Domain}/api/v2/users/{userId}";

            // Create an authorisation token for the API request
            var token = CreateToken();

            // Make the GET request to the Auth0 'Users by Id' API path
            var client = new RestClient(usersByIdApiPath);
            var getRequest = new RestRequest(Method.GET);
            getRequest.AddHeader("authorization", $"Bearer {token.access_token}");

            IRestResponse response = client.Execute(getRequest);

            // Get the JSON reponse and store as a User object
            Auth0User user = JsonConvert.DeserializeObject<Auth0User>(response.Content);

            // Return User object
            return user;
        }

        /// <summary>
        /// search for users
        /// </summary>
        public List<Auth0User> SearchUsers(string name, string email)
        {
            string usersByIdApiPath = $"https://{_auth0.Domain}/api/v2/users?q=";
            if (!string.IsNullOrEmpty(name))
            {
                //usersByIdApiPath = $"{usersByIdApiPath}name:\"*{name}*\"&";
                usersByIdApiPath = $"{usersByIdApiPath}name:*{name}*&";
            }
            if (!string.IsNullOrEmpty(email))
            {
                usersByIdApiPath = $"{usersByIdApiPath}email:*{email}*&";
            }

            usersByIdApiPath = $"{usersByIdApiPath}search_engine=v3";

            // Create an authorisation token for the API request
            var token = CreateToken();

            // Make the GET request to the Auth0 'Users by Id' API path
            var client = new RestClient(usersByIdApiPath);
            var getRequest = new RestRequest(Method.GET);
            getRequest.AddHeader("authorization", $"Bearer {token.access_token}");

            IRestResponse response = client.Execute(getRequest);

            List<Auth0User> userList = null;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                // Get the JSON reponse and store as a User object
                userList = JsonConvert.DeserializeObject<List<Auth0User>>(response.Content);

            }

            // Return User object
            return userList;
        }

        /// <summary>
        /// password, 2 unique chars, 2 upper case
        /// </summary>
        private string GeneratePassword()
        {
            string uniqueChars = "!@#$%^&*";
            string basePass = Guid.NewGuid().ToString().ToLower().Replace("-", "").Substring(1, 15);
            string upperPass = string.Empty;

            // 2 upper case
            int uppI = 0;
            for (int i = 0; i < basePass.Length; i++)
            {
                char c = basePass[i];
                if (char.IsLetter(c) && uppI < 2)
                {
                    upperPass += char.ToUpper(c);
                    uppI++;
                }
                else
                {
                    upperPass += c;
                }
            }

            // 2 unique chars
            Random random = new Random();
            for (int i = 0; i < 2; i++)
            {
                char u = uniqueChars[random.Next(uniqueChars.Length)];
                upperPass = upperPass.Insert(random.Next(upperPass.Length), u.ToString());
            }

            return upperPass;
        }

        /// <summary>
        /// Creates an Auth0 User
        /// </summary>
        public Auth0Response CreateUser(Auth0User user)
        {
            Auth0Response ar = new Auth0Response();

            if (string.IsNullOrEmpty(user.password))
            {
                // random password
                user.password = GeneratePassword();
            }

            string createUserApiPath = $"https://{_auth0.Domain}/api/v2/users";
            string userParams = $"{{\"email\":\"{user.email}\",\"given_name\":\"{user.given_name}\",\"family_name\":\"{user.family_name}\",\"name\":\"{user.name}\",\"connection\":\"{_auth0.DBName}\",\"password\":\"{user.password}\"}}";

            // Create an authorisation token for the API request
            var token = CreateToken();

            // Make the POST request to the Auth0 'Create User' API path
            var client = new RestClient(createUserApiPath);
            var postRequest = new RestRequest(Method.POST);
            postRequest.AddHeader("authorization", $"Bearer {token.access_token}");
            postRequest.AddParameter("application/json", userParams, RestSharp.ParameterType.RequestBody);

            IRestResponse response = client.Execute(postRequest);

            if (response.StatusCode == HttpStatusCode.Conflict)    // existing user found
            {
                var existUser = GetUserByEmail(user.email);
                if (existUser != null)
                {
                    ar.Message = "Existing user";
                    ar.User = existUser;
                    ar.IsSuccessful = true;
                }
                else
                {
                    // existing user not found...
                    ar.Message = "Existing user for the email cannot be created or found.";
                    ar.IsSuccessful = false;
                }
            }
            else
            {
                // created a user
                ar.IsSuccessful = response.IsSuccessful;
                if (!ar.IsSuccessful)
                {
                    var are = JsonConvert.DeserializeObject<Auth0ResponseError>(response.Content);
                    ar.Message = are.message;
                }
                else
                {
                    ar.Message = "User created";
                    ar.User = JsonConvert.DeserializeObject<Auth0User>(response.Content);
                }
            }
            return ar;
        }

        /// <summary>
        /// Updates an Auth0 User
        /// </summary>
        public Auth0Response UpdateUser(Auth0User user)
        {
            Auth0Response ar = new Auth0Response();
            string updateUserApiPath = $"https://{_auth0.Domain}/api/v2/users/{user.user_id}";
            string userParams = $"{{\"name\":\"{user.name}\",\"given_name\":\"{user.given_name}\",\"family_name\":\"{user.family_name}\",\"picture\":\"{user.picture}\",\"connection\":\"{_auth0.DBName}\"}}";

            // Create an authorisation token for the API request
            var token = CreateToken();

            // Make the PATCH request to the Auth0 'Update User' API path
            var client = new RestClient(updateUserApiPath);
            var postRequest = new RestRequest(Method.PATCH);
            postRequest.AddHeader("authorization", $"Bearer {token.access_token}");
            postRequest.AddParameter("application/json", userParams, RestSharp.ParameterType.RequestBody);

            IRestResponse response = client.Execute(postRequest);

            ar.IsSuccessful = response.IsSuccessful;
            if (!ar.IsSuccessful)
            {
                var are = JsonConvert.DeserializeObject<Auth0ResponseError>(response.Content);
                ar.Message = are.message;
            }
            else
            {
                ar.Message = "User Updated";
                ar.User = JsonConvert.DeserializeObject<Auth0User>(response.Content);
            }
            return ar;
        }

        /// <summary>
        /// Deletes an Auth0 User
        /// </summary>
        public Auth0Response DeleteUser(string userId)
        {
            Auth0Response ar = new Auth0Response();
            string deleteUserApiPath = $"https://{_auth0.Domain}/api/v2/users/{userId}";

            // Create an authorisation token for the API request
            var token = CreateToken();

            // Make the DELETE request to the Auth0 'Delete User' API path
            var client = new RestClient(deleteUserApiPath);
            var deleteRequest = new RestRequest(Method.DELETE);
            deleteRequest.AddHeader("authorization", $"Bearer {token.access_token}");

            IRestResponse response = client.Execute(deleteRequest);

            ar.IsSuccessful = response.IsSuccessful;
            if (!ar.IsSuccessful)
            {
                var are = JsonConvert.DeserializeObject<Auth0ResponseError>(response.Content);
                ar.Message = are.message;
            }
            return ar;
        }



    }

    #region auth0 classes
    public class Auth0Token
    {
        public string access_token { get; set; }
        public string scope { get; set; }
        public int expires_in { get; set; }
        public string token_type { get; set; }
    }

    public class Auth0User
    {
        public DateTime created_at { get; set; }
        public string email { get; set; }
        public string phone_number { get; set; }
        public bool blocked { get; set; }
        public bool email_verified { get; set; }
        public bool phone_verified { get; set; }
        public Auth0Identity[] identities { get; set; }
        public string given_name { get; set; }
        public string family_name { get; set; }
        public string name { get; set; }
        public string nickname { get; set; }
        public string picture { get; set; }
        public DateTime updated_at { get; set; }
        public string user_id { get; set; }
        public string password { get; set; }
        public string username { get; set; }
        public string connection { get; set; }
    }

    public class Auth0Identity
    {
        public string connection { get; set; }
        public string user_id { get; set; }
        public string provider { get; set; }
        public bool isSocial { get; set; }
    }

    public class Auth0Response
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }

        // create a user returns this is successful
        public Auth0User User { get; set; }
    }

    public class Auth0ResponseError
    {
        public int statusCode { get; set; }
        public string error { get; set; }
        public string message { get; set; }
        public string errorCode { get; set; }
    }

    #endregion
}
