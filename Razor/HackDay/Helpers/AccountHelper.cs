﻿using Amazon;
using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using HackDay.Classes;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HackDay.Helpers
{
    public class AccountHelper
    {
        public AccountModel _account { get; set; }

        // Constructor
        public AccountHelper()
        {
            _account = LoadFromParamStore();
            if (_account == null)
            {
                _account = LoadFromFile();
            }
        }

        /// <summary>
        /// load config from the parameter store
        /// </summary>
        private AccountModel LoadFromParamStore()
        {
            AccountModel account = null;

            // get from app setting in the config
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path, false);
            var root = configurationBuilder.Build();
            var appSetting = root.GetSection("AppSettings");
            var paramStoreProfile = appSetting["ParamStoreProfile"];
            var paramStoreKey = appSetting["ParamStoreKey"];

            var request = new GetParametersRequest
            {
                Names = new List<string> { paramStoreKey }
            };

            try
            {
                CredentialProfile basicProfile;
                AWSCredentials awsCredentials;
                var sharedFile = new SharedCredentialsFile();
                if (sharedFile.TryGetProfile(paramStoreProfile, out basicProfile) &&
                    AWSCredentialsFactory.TryGetAWSCredentials(basicProfile, sharedFile, out awsCredentials))
                {
                    using (var ssm = new AmazonSimpleSystemsManagementClient(awsCredentials, RegionEndpoint.APSoutheast2))
                    {
                        var response = ssm.GetParametersAsync(request).Result;
                        if (response.Parameters.Count > 0)
                        {
                            string retValue = response.Parameters.ToArray()[0].Value;
                            account = JsonConvert.DeserializeObject<AccountModel>(retValue);
                        }
                    }
                }
            }
            catch (Exception)
            {
                account = null;
            }
            return account;
        }

        /// <summary>
        /// load config from file
        /// </summary>
        private AccountModel LoadFromFile()
        {
            AccountModel account = null;
            string configFileName = @"config.json";
            string projectRootPath = AppDomain.CurrentDomain.BaseDirectory;
            string configFileAndPath = projectRootPath + configFileName;

            try
            {
                using (StreamReader r = new StreamReader(configFileAndPath))
                {
                    string retValue = r.ReadToEnd();
                    account = JsonConvert.DeserializeObject<AccountModel>(retValue);
                }
            }
            catch (Exception)
            {
                account = null;
            }
            return account;
        }

        /// <summary>
        /// Searches for a SAM User by email address and returns a SamUser object
        /// </summary>
        public SamUser GetSamUserByEmail(string emailAddress)
        {
            string apiPath = $"{_account.SamDomain}/account/{emailAddress}";

            // Make the GET request to the API path
            var client = new RestClient(apiPath);
            var getRequest = new RestRequest(Method.GET);
            getRequest.AddHeader("x-api-key", _account.SamApiKey);
            getRequest.AddHeader("content-type", _account.SamContentType);

            IRestResponse response = client.Execute(getRequest);

            // Get the JSON reponse and store as a SamUser object
            SamUser samUser = JsonConvert.DeserializeObject<SamUser>(response.Content);

            // Return SamUser object
            return samUser;
        }

        /// <summary>
        /// Enables/Disables a SAM User
        /// </summary>
        public AccountResponse SetSamUserActive(SamUser samUser)
        {
            var ar = new AccountResponse();
            var upn = samUser.Active == false ? "" : samUser.UPN;

            string apiPath = $"{_account.SamDomain}/account/update/email";
            string userParams = $"{{\"Email\":\"{samUser.Email}\",\"UPN\":\"{upn}\",\"Active\":{samUser.Active}}}";

            // Make the PATCH request to the API path
            var client = new RestClient(apiPath);
            var postRequest = new RestRequest(Method.POST);
            postRequest.AddHeader("x-api-key", _account.SamApiKey);
            postRequest.RequestFormat = DataFormat.Json;
            postRequest.AddHeader("content-type", _account.SamContentType);
            //postRequest.AddParameter("Email", samUser.Email);
            //postRequest.AddParameter("UPN", upn);
            //postRequest.AddParameter("Email", samUser.Active);
             postRequest.AddJsonBody(new { Email = samUser.Email, UPN = upn, Active= samUser.Active });

            IRestResponse response = client.Execute(postRequest);

            ar.IsSuccessful = response.IsSuccessful;
            if (!ar.IsSuccessful)
            {
                var are = JsonConvert.DeserializeObject<AccountResponseError>(response.Content);
                ar.Message = are.message;
            }
            else
            {
                ar.Message = "SAM User Updated";
            }
            return ar;
        }
    }

    #region Account Classes
    public class SamUser
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string UPN { get; set; }
        public bool Active { get; set; }
    }


    public class AccountResponse
    {
        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
    }

    public class AccountResponseError
    {
        public int statusCode { get; set; }
        public string error { get; set; }
        public string message { get; set; }
        public string errorCode { get; set; }
    }
    #endregion
}
